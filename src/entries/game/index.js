import dva from 'dva';
import $ from 'jquery';
import Clipboard from 'clipboard';
import router from './router';
import models from '../../game/models';
import { message } from '../../lib/antd';
import { QUERYS } from '../../game/constants';


window.clipboard = new Clipboard('.clipboard-target');
window.clipboard.on('success', (e) => {
  message.success('复制成功');
  e.clearSelection();
});

window.clipboard.on('error', () => {
  message.success('复制失败，请尝试手工复制');
});


const locale = localStorage.getItem('BRB_LOCAL') || 'zh-CN';
window.locale = locale;

function render() {
  $.ajax(QUERYS.I18N(locale)).done((data) => {
    // 设置一个全局变量
    window.i18n = data;
    // 初始化
    const app = dva();
    Object.keys(models).forEach((key) => {
      app.model(models[key]);
    });
    app.router(router);
    app.start('#app');
  });
}

if (!global.Intl) {
  require.ensure([
    'intl',
  ], (require) => {
    require('intl');

    render();
  });
} else {
  render();
}
