import React from 'react';
import { routerRedux, Route, Switch, Redirect } from 'dva/router';

// 优先加载antd样式
import '../../lib/antd/style.less';

import Main from '../../game/layout/main';  // 主视图
import MainCom from '../../game/components/main'; // eos投注

const { ConnectedRouter } = routerRedux;

export default function Routers({ history }) {
  return (
    <ConnectedRouter history={history}>
      <Main>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/eos_bet" />} />
          <Route exact path="/eos_bet" component={MainCom} />
        </Switch>
      </Main>
    </ConnectedRouter>
  );
}
