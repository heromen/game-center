import React, { Component } from 'react';
import { connect } from 'dva';
import { Button, Form, Input, Slider, Modal, Radio, Popconfirm, message } from 'antd';
import { FormattedMessage } from 'react-intl';
import $ from 'jquery';

import { QUERYS } from '../../constants';

const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;

const inviteRate = 1.005;

// https://dice.eosbet.io/assets/img/eos-logo.png
class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sliderValue: 50,
      betAmount: 1,
      formValue: {
        payoutOnWin: 2 * inviteRate,
        payout: 2 * inviteRate,
        winChange: 49,
      },
      isButton: true,
      randomNum: Math.ceil(Math.random() * 100),
      loading: false,
      playModal: false,
    };
  }
  componentDidMount() {
    this.queryAssets();
  }
  queryAssets() {
    $.ajax({
      url: QUERYS.QUERY_ASSETS,
    })
      .done((data) => {
        const eosObj = data.data.filter(d => d.code === 'EOS')[0];
        this.props.dispatch({
          type: 'eos/updateState',
          payload: {
            eosBalance: eosObj.balance,
          },
        });
      })
      .catch((err) => {
        if (err.status === 401) {
          this.props.dispatch({
            type: 'utils/updateState',
            payload: {
              isLogin: false,
            },
          });
        }
      });
  }
  changeSlider(value) {
    if (value > 2 && value < 97) {
      const { formValue, betAmount } = this.state;
      formValue.winChange = value - 1;
      formValue.payout = 98 / formValue.winChange * inviteRate;
      formValue.payoutOnWin = formValue.payout * betAmount;
      this.setState({
        sliderValue: value,
      });
    }
  }
  handleUpdateWin(e) {
    const value = e.target.value;
    if (isNaN(value)) return;
    const { formValue } = this.state;
    formValue.payoutOnWin = value * formValue.payout;
    this.setState({ formValue, betAmount: value });
  }
  handleBet(e) {
    const { eosBalance } = this.props;
    const { formValue } = this.state;
    const value = e.target.value;
    const betAmount = value === '1' ? eosBalance / 2 : eosBalance;
    formValue.payoutOnWin = betAmount * formValue.payout;
    this.setState({
      betAmount,
      formValue,
    });
  }
  submitForm() {
    const { betAmount, sliderValue } = this.state;
    const { i18n, dispatch, currentGameResults } = this.props;
    if (betAmount) {
      this.setState({ loading: true });
      dispatch({
        type: 'eos/playGame',
        payload: {
          amount: betAmount,
          memo: sliderValue.toString(),
        },
        onSuccess: () => {
          let interval;
          const queryResults = () => {
            this.setState({ isButton: false });
            interval = setInterval(() => this.setState({
              randomNum: Math.ceil(Math.random() * 100),
            }), 100);
            setTimeout(() => {
              clearInterval(interval);
              this.setState({ isButton: true });
              dispatch({
                type: 'eos/queryCurrentGameResults',
                onSuccess: (d) => {
                  const newResult = d.filter(e => !JSON.stringify(currentGameResults).includes(JSON.stringify(e)));
                  if (newResult && newResult.length) {
                    const roll = newResult[0].roll;
                    if (roll && roll < sliderValue) {
                      Modal.success({
                        title: i18n.game_eos_tips_congratulation,
                        content: `${i18n.game_eos_my_bets}: ${sliderValue}, ${i18n.game_eos_tips_result}: ${roll}`,
                      });
                    } else if (roll && roll >= sliderValue) {
                      Modal.error({
                        title: i18n.game_eos_tips_sorry,
                        content: `${i18n.game_eos_my_bets}: ${sliderValue}, ${i18n.game_eos_tips_result}: ${roll}`,
                      });
                    }
                    this.queryAssets();
                    this.setState({ loading: false });
                    dispatch({
                      type: 'eos/updateState',
                      payload: {
                        currentGameResults: d,
                      },
                    });
                  } else {
                    message.error(i18n.game_eos_slow_network);
                    queryResults();
                  }
                },
              });
            }, 4000);
          };
          queryResults();
        },
        onFail: () => {
          this.setState({
            loading: false,
          });
        },
      });
    } else {
      message.error(i18n.game_eos_bet_amount);
    }
  }
  render() {
    const { sliderValue, formValue, playModal, betAmount, isButton, randomNum, loading } = this.state;
    const { i18n, eosBalance } = this.props;
    const radioAfter = (
      <RadioGroup onChange={this.handleBet.bind(this)}>
        <RadioButton value="1">1/2</RadioButton>
        <RadioButton value="2">MAX</RadioButton>
      </RadioGroup>
    );
    const marks = {
      3: 3,
      96: 96,
    };
    return (
      <div id="game">
        <div className="container">
          <a onClick={() => this.setState({ playModal: true })} className="game-how"><FormattedMessage id="game_eos_rule_title" /></a>
          <div className="form-row d-flex flex-wrap">
            <div className="input-row d-flex">
              <span className="game-small"><FormattedMessage id="game_eos_bet_amount" /> </span>
              <Input type="number" min="0.1" value={betAmount} addonAfter={radioAfter} onChange={this.handleUpdateWin.bind(this)} />
            </div>
            <div className="input-row d-flex">
              <span className="game-small"><FormattedMessage id="game_eos_payout_on_win" /> </span>
              <div>
                <img className="navbar-coin" src="https://assets.bitrabbit.com/upload/1733d322-fdca-4ae2-91eb-8b17b9564b78.png" role="presentation" width="14" />
                <span className="navbar-coin-value">{parseFloat(formValue.payoutOnWin).toFixed(4)}</span>
              </div>
            </div>
          </div>
          <div className="game-middle-wrapper flex-wrap d-flex">
            <div className="game-middle-roll">
              <span className="game-small"><FormattedMessage id="game_eos_roll_under" /></span>
              <div className="game-middle-roll-value f20">{sliderValue}↓</div>
            </div>
            <div className="game-middle-payout">
              <span className="game-small"><FormattedMessage id="game_eos_payout" /></span>
              <div className="game-middle-payout-value f20">{Math.round(formValue.payout * 100) / 100}x</div>
            </div>
            <div className="game-middle-chance">
              <span className="game-small"><FormattedMessage id="game_eos_win_chance" /></span>
              <div className="game-middle-chance-value f20">{parseFloat(formValue.winChange).toFixed(2)}%</div>
            </div>
          </div>
          <div className="d-flex flex-wrap login-wrapper">
            <div className="eos-balance">
              <span><img className="navbar-coin" src="https://assets.bitrabbit.com/upload/1733d322-fdca-4ae2-91eb-8b17b9564b78.png" role="presentation" width="18" /> {eosBalance} </span>
              <a href="https://bitrabbit.com/dashboard/#/assets" target="_blank" rel="noopener noreferrer"><FormattedMessage id="game_eos_goto_deposit" /></a>
            </div>
            <div className="login-btn">
              {isButton ?
                <Popconfirm placement="top" title={<FormattedMessage id="game_eos_tips_sure" />} onConfirm={this.submitForm.bind(this)}><Button loading={loading} className={window.locale} type="primary" size="large" block><FormattedMessage id="game_eos_bet" /></Button></Popconfirm> :
                <Button type="primary" size="large" block>{randomNum}</Button>}
            </div>
          </div>
        </div>
        <div className="slider-wrapper">
          <Slider defaultValue={50} marks={marks} value={sliderValue} onChange={this.changeSlider.bind(this)} />
        </div>
        <Modal
          visible={playModal}
          title={<FormattedMessage id="game_eos_rule_title" />}
          onCancel={() => this.setState({ playModal: false })}
          footer={false}
          className="modal-tips"
        >
          <ol>
            <li>{i18n.game_eos_rule_1}<a href="https://bitrabbit.com/dashboard/#/assets" target="_blank" rel="noopener noreferrer">{i18n.game_eos_rule_deposite}</a></li>
            <li>{i18n.game_eos_rule_2}</li>
            <li>{i18n.game_eos_rule_3}</li>
            <li>{i18n.game_eos_rule_4}</li>
            <li>{i18n.game_eos_rule_5}</li>
            <li>{i18n.game_eos_rule_6}</li>
          </ol>
        </Modal>
      </div>
    );
  }
}


function mapStateToProps({ utils, eos }) {
  return {
    i18n: utils.i18n,
    eosBalance: eos.eosBalance,
    currentGameResults: eos.currentGameResults,
  };
}

export default connect(mapStateToProps)(Form.create()(Game));
