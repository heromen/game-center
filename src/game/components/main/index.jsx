import React, { Component } from 'react';
import { connect } from 'dva';
import './style.scss';
import Game from './game';
import List from './list';

import SocketEOS from './websocket';

class MainCom extends Component {
  render() {
    return (
      <div className="eos" id="eos">
        <Game />
        <List />
        <SocketEOS />
        <div className="wx">
          <div className="qrcode">
            <img src="https://assets.bitrabbit.com/upload/79e525ed-f4cf-411b-b9c0-cee9d718ab26.png" width="250" alt="qrcode" />
          </div>
          <img src="https://assets.bitrabbit.com/upload/a9c3b36c-a4e8-4298-b230-0c8bdd6543b1.svg" role="presentation" />
        </div>
      </div>
    );
  }
}

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(MainCom);
