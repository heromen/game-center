import React, { Component } from 'react';
import { connect } from 'dva';
import { Tabs, Table } from 'antd';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';

const TabPane = Tabs.TabPane;

// const tabsMap = ['game_eos_all_bets', 'game_eos_my_bets', 'game_eos_huge_wins', 'game_eos_top_bettors'];
const tabsMap = ['game_eos_all_bets', 'game_eos_my_bets'];

class List extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'time',
        title: <FormattedMessage id="game_eos_time" />,
        dataIndex: 'time',
        render: t => moment(t).format('YYYY-MM-DD HH:mm:ss'),
        className: 'mobile',
      }, {
        key: 'bettor',
        title: <FormattedMessage id="game_eos_bettor_lower" />,
        dataIndex: 'bettor',
      }, {
        key: 'roll_under',
        title: <FormattedMessage id="game_eos_roll_under_lower" />,
        dataIndex: 'roll_under',
        className: 'mobile',
      }, {
        key: 'bet',
        title: <FormattedMessage id="game_eos_bet_lower" />,
        dataIndex: 'bet',
        render: t => <span>{`${t} EOS`}</span>,
        className: 'mobile',
      }, {
        key: 'roll',
        title: <FormattedMessage id="game_eos_roll" />,
        dataIndex: 'roll',
        className: 'mobile',
      }, {
        key: 'payout',
        title: <FormattedMessage id="game_eos_payout_on_win" />,
        dataIndex: 'payout',
        render: t => (t ? <span className="text-success">{`${t} EOS`}</span> : ''),
      },
    ];
    this.topColumns = [
      {
        key: 'bettor',
        dataIndex: 'bettor',
        title: <FormattedMessage id="game_eos_bettor_lower" />,
      }, {
        key: 'wagered',
        dataIndex: 'wagered',
        title: <FormattedMessage id="game_eos_wagered" />,
      },
    ];
  }
  selectSource(i) {
    let result = [];
    const { currentGameResults, gameResults } = this.props;
    switch (i) {
      case 0:
        result = gameResults;
        break;
      case 1:
        result = currentGameResults;
        break;
      case 2:
        result = [];
        break;
      case 3:
        result = [];
        break;
      default:
        break;
    }
    return result;
  }
  render() {
    return (
      <div id="list">
        <div className="container">
          <Tabs defaultActiveKey="0">
            {tabsMap.map((t, i) => (
              <TabPane tab={<FormattedMessage id={t} />} key={i}>
                <Table
                  columns={i < 3 ? this.columns : this.topColumns}
                  pagination={false}
                  dataSource={this.selectSource(i)}
                  rowKey={r => r.id}
                />
              </TabPane>
            ))}
          </Tabs>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ eos }) {
  const { currentGameResults, gameResults } = eos;
  return {
    currentGameResults,
    gameResults,
  };
}

export default connect(mapStateToProps)(List);
