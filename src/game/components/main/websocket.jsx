import React, { Component } from 'react';
import ActionCable from 'actioncable';
import { connect } from 'dva';

// let socketEos;
const wsUrl = 'wss://bitrabbit.com/cable';

class SocketEOS extends Component {
  componentDidMount() {
    this.init();
  }
  init() {
    // socketEos = document.getElementById('socketEos');
    this.handleSocketEos();
  }
  handleSocketEos() {
    const cable = ActionCable.createConsumer(wsUrl);
    cable.subscriptions.create('GameResultsChannel', {
      connected() {
        console.log('connected');
      },
      disconnected() {
        console.log('disconnected');
      },
      received: (data) => {
        if (data) {
          console.log('websocket: ', data);
          const { gameResults } = this.props;
          if (gameResults.length && gameResults.length >= 30) {
            gameResults.pop();
          }
          gameResults.unshift(data[1]);
          setTimeout(() => {
            this.props.dispatch({
              type: 'eos/queryGameResults',
            });
          }, 2000);
        }
      },
      rejected: () => {
        console.log('rejected');
      },
    });
  }
  render() {
    return <div id="socketEos" />;
  }
}

function mapStateToProps({ eos }) {
  return {
    gameResults: eos.gameResults,
  };
}

export default connect(mapStateToProps)(SocketEOS);
