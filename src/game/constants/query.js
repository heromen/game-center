// import moment from 'moment';

function getSign() {
  const d = new Date();
  return parseInt(d / 1000 / 60 / 60, 10) * 1000 * 60 * 60;
}

const querys = {
  QUERY_ACCOUNT_BASEINFO: '/web/settings.json',
  QUERY_ASSETS: '/web/accounts.json',
  I18N: locale => `https://assets.bitrabbit.com/i18n/${__ENV__}/game/${locale}.json?_=${getSign()}`,
  PLAY_EOS_GAME: '/web/game/games/play_game',
  QUERY_GAME_RESULTS: '/web/game/games/game_results',
  QUERY_CURRENT_GAME_RESULTS: '/web/game/games/current_game_results',
};

export default querys;
