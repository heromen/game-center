import React, { Component } from 'react';
import { connect } from 'dva';
import { FormattedMessage } from 'react-intl';
import $ from 'jquery';
import { Menu, Dropdown } from '../../../lib/antd';

import './style.scss';

const localeMap = {
  en: 'English',
  'zh-CN': '简体中文',
  // 'zh-TW': '繁體中文',
};

class Header extends Component {
  getLocaleMenu() {
    const menu = (
      <Menu>
        {Object.keys(localeMap).map((locale, i) => (
          <Menu.Item key={i}>
            <a onClick={this.handleChangeLocale.bind(this, locale)}>{localeMap[locale]}</a>
          </Menu.Item>
        ))}
      </Menu>
    );
    return menu;
  }
  handleChangeLocale(locale) {
    this.props.dispatch({
      type: 'utils/changeLocale',
      payload: locale,
    });
  }
  toggleMenu() {
    $('.header-menu ol').toggleClass('show');
  }
  render() {
    const { i18n, locale, isLogin } = this.props;
    return (
      <header>
        <div>
          <div className="logo-container" onClick={() => location.href = '/'}>
            <img src="https://assets.bitrabbit.com/static/4fb9f726bb115a56a574bd954f5e0a84.svg" alt="BitRabbit" />
          </div>
          <div className="header-menu">
            <div className="menu-btn" onClick={this.toggleMenu.bind(this)}><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB0PSIxNTIyNjAwMDM3NDgyIiBjbGFzcz0iaWNvbiIgc3R5bGU9IiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjE5MDYiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PC9zdHlsZT48L2RlZnM+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTY1LjAxMjE3OCAxMjguNTQ0NjU1IDY1LjAxMjE3OCAyMzAuODc1Mjc1IDk1OS4zODE3OTUgMjMwLjg3NTI3NSA5NTkuMzgxNzk1IDEyOC41NDQ2NTUgNjUuMDEyMTc4IDEyOC41NDQ2NTVaIiBwLWlkPSIxOTA3Ij48L3BhdGg+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTY0LjcyOTc0NSA1NjMuMjAxMTI2IDk1OS4yNjkyMzEgNTYzLjIwMTEyNiA5NTkuMjY5MjMxIDQ2MC44NzA1MDYgNjQuNzI5NzQ1IDQ2MC44NzA1MDYgNjQuNzI5NzQ1IDU2My4yMDExMjZaIiBwLWlkPSIxOTA4Ij48L3BhdGg+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTY0LjcyOTc0NSA4OTUuNTI1OTUzIDk1OS4yNjkyMzEgODk1LjUyNTk1MyA5NTkuMjY5MjMxIDc5My4xOTUzMzQgNjQuNzI5NzQ1IDc5My4xOTUzMzQgNjQuNzI5NzQ1IDg5NS41MjU5NTNaIiBwLWlkPSIxOTA5Ij48L3BhdGg+PHBhdGggZD0iTTY0LjcyOTc0NSA4ODkuNzQyMjI3IiBwLWlkPSIxOTEwIj48L3BhdGg+PC9zdmc+" alt="menu" /></div>
            <ol>
              <li><a rel="noopener noreferrer" target="_blank" href="https://pub.bitrabbit.com">{i18n.bunnyPub}</a></li>
              <li><a rel="noopener noreferrer" target="_blank" href={`/download/${locale.toLowerCase()}.html`}>{i18n.app}</a></li>
              <li><a rel="noopener noreferrer" target="_blank" href={'/markets/eth_btc'}>{i18n.markets}</a></li>
              {/* <li><a rel="noopener noreferrer" target="_blank" href={`https://support.bitrabbit.com/hc/${locale.toLowerCase()}`}>{i18n.help}</a></li> */}
              {/* <li><a rel="noopener noreferrer" target="_blank" href={`https://support.bitrabbit.com/hc/${locale.toLowerCase()}/categories/360000063632-%E5%85%AC%E5%91%8A%E4%B8%AD%E5%BF%83`}>{i18n.announcements}</a></li> */}
              <li className="pc">
                <Dropdown overlay={this.getLocaleMenu()} placement="bottomRight" className="language-dropdown">
                  <a className="header-search header-btn" id="localeSelector">
                    <span>{localeMap[locale]}</span>
                  </a>
                </Dropdown>
              </li>
              {Object.keys(localeMap).map((l, i) => (
                <li className="mobile" key={i}>
                  <a onClick={this.handleChangeLocale.bind(this, l)}>{localeMap[l]}</a>
                </li>
              ))}
              {!isLogin && <li>
                <a href={`/${window.locale.toLowerCase()}?open=signin&redirect_to=${encodeURIComponent(window.location.pathname + window.location.search + window.location.hash)}`} ><FormattedMessage id="signin" /></a>/
                <a href={`/${window.locale.toLowerCase()}?open=signup&redirect_to=${encodeURIComponent(window.location.pathname + window.location.search + window.location.hash)}`} ><FormattedMessage id="signup" /></a>
              </li>}
            </ol>
          </div>
        </div>
      </header>
    );
  }
}

function mapStateToProps({ utils }) {
  return {
    locale: utils.locale,
    i18n: utils.i18n,
    isLogin: utils.isLogin,
  };
}

export default connect(mapStateToProps)(Header);
