

import pathToRegexp from 'path-to-regexp';
import fetch from '../../lib/fetch';
// import { message } from '../../lib/antd';
import { QUERYS } from '../constants';

const queryBaseInfo = () => fetch.get(QUERYS.QUERY_ACCOUNT_BASEINFO);
const playGame = payload => fetch.post(QUERYS.PLAY_EOS_GAME, payload);
const queryCurrentGameResults = () => fetch.get(QUERYS.QUERY_CURRENT_GAME_RESULTS);
const queryGameResults = () => fetch.get(QUERYS.QUERY_GAME_RESULTS);

export default {
  namespace: 'eos',
  state: {
    baseInfo: {},
    eosBalance: 0,
    currentGameResults: [],
    gameResults: [],
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname }) => {
        const eos = pathToRegexp('/eos_bet').exec(pathname);
        if (eos) {
          dispatch({ type: 'queryBaseInfo' });
          dispatch({ type: 'queryCurrentGameResults' });
          dispatch({ type: 'queryGameResults' });
        }
      });
    },
  },
  effects: {
    * queryBaseInfo(_, { call, put }) {
      let data = yield call(queryBaseInfo);
      if (!data.success) return;
      data = data.data;
      yield put({
        type: 'updateState',
        payload: {
          baseInfo: data,
        },
      });
    },
    * playGame({ payload, onSuccess, onFail }, { call }) {
      const params = { ...payload };
      params.currency = 'EOS';
      params.play_currency = 'BET';
      const data = yield call(playGame, params);
      if (data.success) onSuccess();
      else onFail();
    },
    * queryCurrentGameResults({ onSuccess }, { call, put }) {
      const data = yield call(queryCurrentGameResults);
      if (data.success) {
        if (onSuccess) {
          onSuccess(data.data || []);
          return;
        }
        yield put({
          type: 'updateState',
          payload: {
            currentGameResults: data.data || [],
          },
        });
      }
    },
    * queryGameResults({ onSuccess }, { call, put }) {
      const data = yield call(queryGameResults);
      if (data.success) {
        const result = data.data;
        if (onSuccess) {
          onSuccess(result || []);
          return;
        }
        yield put({
          type: 'updateState',
          payload: {
            gameResults: result || [],
          },
        });
      }
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
