import utils from './utils';
import eos from './eos';

const reducers = {
  utils,
  eos,
};

export default reducers;
