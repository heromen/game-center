import { routerRedux } from 'dva/router';
import $ from 'jquery';
// import pathToRegexp from 'path-to-regexp';
import { QUERYS } from '../constants';

const push = routerRedux.push;

const changeLocale = locale => () => new Promise((resolve) => {
  $.ajax(QUERYS.I18N(locale)).done(data => resolve(data));
});

export default {
  namespace: 'utils',
  state: {
    pathname: '',
    history: null,
    locale: '',
    i18n: {},
    acitveRoute: null,
    isLogin: true,
  },
  subscriptions: {
    setup({ dispatch }) {
      // 设置locale
      dispatch({
        type: 'updateState',
        payload: {
          locale: window.locale,
          i18n: window.i18n,
        },
      });
    },
  },
  effects: {
    * goBack(_, { select }) {
      const history = yield select(({ utils }) => utils.history);
      history.goBack();
    },
    * goto({ goto }, { put }) {
      yield put(push(goto));
    },
    * changeLocale({ payload }, { call, select, put }) {
      const origin = yield select(({ utils }) => utils.locale);
      if (origin === payload) return;
      const i18n = yield call(changeLocale(payload));
      if (i18n) {
        // 一些全局变量以及LocalStorage
        window.locale = payload;
        window.i18n = i18n;
        localStorage.setItem('BRB_LOCAL', payload);
        yield put({
          type: 'updateState',
          payload: {
            i18n,
            locale: payload,
          },
        });
      }
    },
  },
  reducers: {
    updateCurrentPathName(state, { pathname, history }) {
      return { ...state, pathname, history };
    },
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
