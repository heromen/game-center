import Button from 'antd/lib/button';
import message from 'antd/lib/message';
import Menu from 'antd/lib/menu';
import Icon from 'antd/lib/icon';
import Breadcrumb from 'antd/lib/breadcrumb';
import Tooltip from 'antd/lib/tooltip';
import Alert from 'antd/lib/alert';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Select from 'antd/lib/select';
import Steps from 'antd/lib/steps';
import Upload from 'antd/lib/upload';
import Table from 'antd/lib/table';
import Divider from 'antd/lib/divider';
import Pagination from 'antd/lib/pagination';
import Radio from 'antd/lib/radio';
import Modal from 'antd/lib/modal';
import Tabs from 'antd/lib/tabs';
import Dropdown from 'antd/lib/dropdown';
import Badge from 'antd/lib/badge';
import Spin from 'antd/lib/spin';
import Switch from 'antd/lib/switch';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Collapse from 'antd/lib/collapse';

export {
  Button,
  message,
  Menu,
  Icon,
  Breadcrumb,
  Tooltip,
  Alert,
  Form,
  Input,
  Select,
  Steps,
  Upload,
  Table,
  Divider,
  Pagination,
  Radio,
  Modal,
  Tabs,
  Dropdown,
  Badge,
  Spin,
  Switch,
  Row,
  Col,
  Collapse,
};
