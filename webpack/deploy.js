var fs = require('fs');

var files;
var reg = /.+\.html/;

const replaceContent = `
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-113346386-1', 'auto');
  ga('send', 'pageview');
</script>
<script type='text/javascript'>
!function(e,t,n,g,i){e[i]=e[i]||function(){(e[i].q=e[i].q||[]).push(arguments)},n=t.createElement("script"),tag=t.getElementsByTagName("script")[0],n.async=1,n.src=('https:'==document.location.protocol?'https://':'http://')+g,tag.parentNode.insertBefore(n,tag)}(window,document,"script","assets.growingio.com/2.1/gio.js","gio");
  gio('init','a09c75ac8ddc1dc8', {});

gio('send');

</script>
  </head>
`;

if (fs.existsSync('./dist/staging/assets')) {
  files = fs.readdirSync('./dist/staging/assets');
  
  files.forEach(function(file) {
    if (reg.test(file)) {
      fs.renameSync('./dist/staging/assets/' + file, './dist/staging/' + file);
      let content = fs.readFileSync('./dist/staging/' + file).toString();
      content = content.replace('</head>', replaceContent);
      fs.writeFileSync('./dist/staging/' + file, content);
    }
  });
}

if (fs.existsSync('./dist/production/assets')) {
  files = fs.readdirSync('./dist/production/assets');
  
  files.forEach(function(file) {
    if (reg.test(file)) {
      fs.renameSync('./dist/production/assets/' + file, './dist/production/' + file);
      let content = fs.readFileSync('./dist/production/' + file).toString();
      content = content.replace('</head>', replaceContent);
      fs.writeFileSync('./dist/production/' + file, content);
    }
  });
}

